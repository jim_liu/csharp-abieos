安裝[cmake](https://cmake.org/)
安裝[mingw-w64](http://mingw-w64.org/doku.php/download)

```sh
cd cpp

# 建立makefile
cmake .
# 或(若用MinGW編譯)
cmake -G "MinGW Makefiles" .

# build libabieos-lib
cmake --build .
```

# Issue

### fatal error: 'string.h' file not found
> macos必須升到10.14或以上 (並重建makefile)

### malloc: *** error for object 0x1018ad6a0: pointer being freed was not allocated
> [參考](https://www.itread01.com/content/1546584542.html)

### windows平台不要用visual studio編譯
> 安裝mingw-w64
> 修改CMakeLists.txt的相關路徑
> 用cmake_gui建立makefile
- 設定source code與build bins路徑
- Configure (選mingw)
- Generate
- 有更動時可以執行Delete Cache

# Reference

[Java範例](https://github.com/EOSIO/eosio-java-android-abieos-serialization-provider/blob/e6e632fd36983e917561b731c0186fdff4bab772/eosiojavaabieos/src/main/java/one/block/eosiojavaabieosserializationprovider/AbiEosSerializationProviderImpl.java)

