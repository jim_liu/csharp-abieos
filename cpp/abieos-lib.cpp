#include "abieos.h"
#include <iostream>
#include <cstring>

#ifdef _WIN32 || _WIN64
#include <windows.h>
#endif

#ifdef _WIN32 || _WIN64
// https://stackoverflow.com/questions/5860882/how-do-i-prevent-accessviolationexception-when-returning-a-string-from-c-to-c
char *MakeStringCopy(const char *str)
{
    std::size_t const len = std::strlen(str);
    HLOCAL const result = ::LocalAlloc(LPTR, len + 1u);

    if (result)
        std::strncpy(static_cast<char *>(result), str, len);
    return (char *)result;
}
#else
// https://www.itread01.com/content/1546584542.html
char *MakeStringCopy(const char *str)
{
    if (str == NULL)
        return NULL;

    char *res = (char *)malloc(strlen(str) + 1);
    strcpy(res, str);
    return res;
}
#endif

extern "C"
{
    abieos_context *em_create()
    {
        abieos_context *context = abieos_create();
        return context;
    }

    void em_destroy(abieos_context *context)
    {
        abieos_destroy(context);
    }

    uint64_t em_string_to_name(abieos_context *context, char *nameStr)
    {
        uint64_t name = abieos_string_to_name(context, nameStr);
        return name;
    }

    const char *em_name_to_string(abieos_context *context, uint64_t name)
    {
        return MakeStringCopy(abieos_name_to_string(context, name));
    }

    bool em_set_abi(abieos_context *context, uint64_t contract, char *abiStr)
    {
        abieos_bool ret = abieos_set_abi(context, contract, abiStr);
        return (bool)ret;
    }

    bool em_json_to_bin(abieos_context *context, uint64_t contract, char *typeStr, char *jsonStr, bool reorderable)
    {
        bool ret;
        if (reorderable)
        {
            ret = abieos_json_to_bin_reorderable(context, contract, typeStr, jsonStr);
        }
        else
        {
            ret = abieos_json_to_bin(context, contract, typeStr, jsonStr);
        }
        return ret;
    }

    const char *em_hexToJson(abieos_context *context, uint64_t contract, char *typeStr, char *jsonStr)
    {
        return MakeStringCopy(abieos_hex_to_json(context, contract, typeStr, jsonStr));
    }

    const char *em_get_type_for_action(abieos_context *context, uint64_t contract, uint64_t action)
    {
        return MakeStringCopy(abieos_get_type_for_action(context, contract, action));
    }

    const int em_get_bin_size(abieos_context *context)
    {
        return abieos_get_bin_size(context);
    }

    const char *em_get_bin_hex(abieos_context *context)
    {
        return MakeStringCopy(abieos_get_bin_hex(context));
    }

    const char *em_get_bin_data(abieos_context *context)
    {
        return MakeStringCopy(abieos_get_bin_data(context));
    }

    const char *em_get_error(abieos_context *context)
    {
        return MakeStringCopy(abieos_get_error(context));
    }
}
