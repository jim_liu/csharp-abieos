﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace eos
{

    static class data
    {
        public static string abi =
         @"{ ""version"": ""eosio::abi/1.1"", ""types"": [ { ""new_type_name"": ""account_name"", ""type"": ""name"" }, { ""new_type_name"": ""action_name"", ""type"": ""name"" }, { ""new_type_name"": ""permission_name"", ""type"": ""name"" } ], ""structs"": [ { ""name"": ""permission_level"", ""fields"": [ { ""name"": ""actor"", ""type"": ""account_name"" }, { ""name"": ""permission"", ""type"": ""permission_name"" } ] }, { ""name"": ""action"", ""fields"": [ { ""name"": ""account"", ""type"": ""account_name"" }, { ""name"": ""name"", ""type"": ""action_name"" }, { ""name"": ""authorization"", ""type"": ""permission_level[]"" }, { ""name"": ""data"", ""type"": ""bytes"" } ] }, { ""name"": ""extension"", ""fields"": [ { ""name"": ""type"", ""type"": ""uint16"" }, { ""name"": ""data"", ""type"": ""bytes"" } ] }, { ""name"": ""transaction_header"", ""fields"": [ { ""name"": ""expiration"", ""type"": ""time_point_sec"" }, { ""name"": ""ref_block_num"", ""type"": ""uint16"" }, { ""name"": ""ref_block_prefix"", ""type"": ""uint32"" }, { ""name"": ""max_net_usage_words"", ""type"": ""varuint32"" }, { ""name"": ""max_cpu_usage_ms"", ""type"": ""uint8"" }, { ""name"": ""delay_sec"", ""type"": ""varuint32"" } ] }, { ""name"": ""transaction"", ""base"": ""transaction_header"", ""fields"": [ { ""name"": ""context_free_actions"", ""type"": ""action[]"" }, { ""name"": ""actions"", ""type"": ""action[]"" }, { ""name"": ""transaction_extensions"", ""type"": ""extension[]"" } ] }, { ""name"": ""callback"", ""fields"": [ { ""name"": ""url"", ""type"": ""string"" }, { ""name"": ""background"", ""type"": ""bool"" } ] }, { ""name"": ""signing_request"", ""fields"": [ { ""name"": ""chain_id"", ""type"": ""variant_id"" }, { ""name"": ""req"", ""type"": ""variant_req"" }, { ""name"": ""broadcast"", ""type"": ""bool"" }, { ""name"": ""callback"", ""type"": ""callback?"" } ] } ], ""variants"": [ { ""name"": ""variant_id"", ""types"": [ ""uint8"", ""checksum256"" ] }, { ""name"": ""variant_req"", ""types"": [ ""action"", ""action[]"", ""transaction"" ] } ] }
        ";

        public static string json =
         @"{""chain_id"":[""checksum256"",""e70aaab8997e1dfce58fbfac80cbbb8fecec7b99cf982a9444273cbc64c41473""],""req"":[""action"",{""account"":""eosio.token"",""name"":""transfer"",""authorization"":[{""actor"":""............1"",""permission"":""............2""}],""data"":""0100000000000000208440C848A3B254102700000000000004475045000000000869206D2074657374""}],""broadcast"":false,""callback"":null}
        ";

        public static string contract = null;
        public static string name = "";
        public static string type = "signing_request";
    }

    // static class data
    // {
    //     public static string abi = "{\"version\":\"eosio::abi/1.1\",\"types\":[],\"structs\":[{\"name\":\"account\",\"base\":\"\",\"fields\":[{\"name\":\"balance\",\"type\":\"asset\"}]},{\"name\":\"close\",\"base\":\"\",\"fields\":[{\"name\":\"owner\",\"type\":\"name\"},{\"name\":\"symbol\",\"type\":\"symbol\"}]},{\"name\":\"create\",\"base\":\"\",\"fields\":[{\"name\":\"issuer\",\"type\":\"name\"},{\"name\":\"maximum_supply\",\"type\":\"asset\"}]},{\"name\":\"currency_stats\",\"base\":\"\",\"fields\":[{\"name\":\"supply\",\"type\":\"asset\"},{\"name\":\"max_supply\",\"type\":\"asset\"},{\"name\":\"issuer\",\"type\":\"name\"}]},{\"name\":\"issue\",\"base\":\"\",\"fields\":[{\"name\":\"to\",\"type\":\"name\"},{\"name\":\"quantity\",\"type\":\"asset\"},{\"name\":\"memo\",\"type\":\"string\"}]},{\"name\":\"open\",\"base\":\"\",\"fields\":[{\"name\":\"owner\",\"type\":\"name\"},{\"name\":\"symbol\",\"type\":\"symbol\"},{\"name\":\"ram_payer\",\"type\":\"name\"}]},{\"name\":\"retire\",\"base\":\"\",\"fields\":[{\"name\":\"quantity\",\"type\":\"asset\"},{\"name\":\"memo\",\"type\":\"string\"}]},{\"name\":\"transfer\",\"base\":\"\",\"fields\":[{\"name\":\"from\",\"type\":\"name\"},{\"name\":\"to\",\"type\":\"name\"},{\"name\":\"quantity\",\"type\":\"asset\"},{\"name\":\"memo\",\"type\":\"string\"}]}],\"actions\":[{\"name\":\"close\",\"type\":\"close\",\"ricardian_contract\":\"---\\nspec_version: \\\"0.2.0\\\"\\ntitle: Close Token Balance\\nsummary: 'Close {{nowrap owner}}’s zero quantity balance'\\nicon: http://127.0.0.1/ricardian_assets/eosio.contracts/icons/token.png#207ff68b0406eaa56618b08bda81d6a0954543f36adc328ab3065f31a5c5d654\\n---\\n\\n{{owner}} agrees to close their zero quantity balance for the {{symbol_to_symbol_code symbol}} token.\\n\\nRAM will be refunded to the RAM payer of the {{symbol_to_symbol_code symbol}} token balance for {{owner}}.\"},{\"name\":\"create\",\"type\":\"create\",\"ricardian_contract\":\"---\\nspec_version: \\\"0.2.0\\\"\\ntitle: Create New Token\\nsummary: 'Create a new token'\\nicon: http://127.0.0.1/ricardian_assets/eosio.contracts/icons/token.png#207ff68b0406eaa56618b08bda81d6a0954543f36adc328ab3065f31a5c5d654\\n---\\n\\n{{$action.account}} agrees to create a new token with symbol {{asset_to_symbol_code maximum_supply}} to be managed by {{issuer}}.\\n\\nThis action will not result any any tokens being issued into circulation.\\n\\n{{issuer}} will be allowed to issue tokens into circulation, up to a maximum supply of {{maximum_supply}}.\\n\\nRAM will deducted from {{$action.account}}’s resources to create the necessary records.\"},{\"name\":\"issue\",\"type\":\"issue\",\"ricardian_contract\":\"---\\nspec_version: \\\"0.2.0\\\"\\ntitle: Issue Tokens into Circulation\\nsummary: 'Issue {{nowrap quantity}} into circulation and transfer into {{nowrap to}}’s account'\\nicon: http://127.0.0.1/ricardian_assets/eosio.contracts/icons/token.png#207ff68b0406eaa56618b08bda81d6a0954543f36adc328ab3065f31a5c5d654\\n---\\n\\nThe token manager agrees to issue {{quantity}} into circulation, and transfer it into {{to}}’s account.\\n\\n{{#if memo}}There is a memo attached to the transfer stating:\\n{{memo}}\\n{{/if}}\\n\\nIf {{to}} does not have a balance for {{asset_to_symbol_code quantity}}, or the token manager does not have a balance for {{asset_to_symbol_code quantity}}, the token manager will be designated as the RAM payer of the {{asset_to_symbol_code quantity}} token balance for {{to}}. As a result, RAM will be deducted from the token manager’s resources to create the necessary records.\\n\\nThis action does not allow the total quantity to exceed the max allowed supply of the token.\"},{\"name\":\"open\",\"type\":\"open\",\"ricardian_contract\":\"---\\nspec_version: \\\"0.2.0\\\"\\ntitle: Open Token Balance\\nsummary: 'Open a zero quantity balance for {{nowrap owner}}'\\nicon: http://127.0.0.1/ricardian_assets/eosio.contracts/icons/token.png#207ff68b0406eaa56618b08bda81d6a0954543f36adc328ab3065f31a5c5d654\\n---\\n\\n{{ram_payer}} agrees to establish a zero quantity balance for {{owner}} for the {{symbol_to_symbol_code symbol}} token.\\n\\nIf {{owner}} does not have a balance for {{symbol_to_symbol_code symbol}}, {{ram_payer}} will be designated as the RAM payer of the {{symbol_to_symbol_code symbol}} token balance for {{owner}}. As a result, RAM will be deducted from {{ram_payer}}’s resources to create the necessary records.\"},{\"name\":\"retire\",\"type\":\"retire\",\"ricardian_contract\":\"---\\nspec_version: \\\"0.2.0\\\"\\ntitle: Remove Tokens from Circulation\\nsummary: 'Remove {{nowrap quantity}} from circulation'\\nicon: http://127.0.0.1/ricardian_assets/eosio.contracts/icons/token.png#207ff68b0406eaa56618b08bda81d6a0954543f36adc328ab3065f31a5c5d654\\n---\\n\\nThe token manager agrees to remove {{quantity}} from circulation, taken from their own account.\\n\\n{{#if memo}} There is a memo attached to the action stating:\\n{{memo}}\\n{{/if}}\"},{\"name\":\"transfer\",\"type\":\"transfer\",\"ricardian_contract\":\"---\\nspec_version: \\\"0.2.0\\\"\\ntitle: Transfer Tokens\\nsummary: 'Send {{nowrap quantity}} from {{nowrap from}} to {{nowrap to}}'\\nicon: http://127.0.0.1/ricardian_assets/eosio.contracts/icons/transfer.png#5dfad0df72772ee1ccc155e670c1d124f5c5122f1d5027565df38b418042d1dd\\n---\\n\\n{{from}} agrees to send {{quantity}} to {{to}}.\\n\\n{{#if memo}}There is a memo attached to the transfer stating:\\n{{memo}}\\n{{/if}}\\n\\nIf {{from}} is not already the RAM payer of their {{asset_to_symbol_code quantity}} token balance, {{from}} will be designated as such. As a result, RAM will be deducted from {{from}}’s resources to refund the original RAM payer.\\n\\nIf {{to}} does not have a balance for {{asset_to_symbol_code quantity}}, {{from}} will be designated as the RAM payer of the {{asset_to_symbol_code quantity}} token balance for {{to}}. As a result, RAM will be deducted from {{from}}’s resources to create the necessary records.\"}],\"tables\":[{\"name\":\"accounts\",\"index_type\":\"i64\",\"key_names\":[],\"key_types\":[],\"type\":\"account\"},{\"name\":\"stat\",\"index_type\":\"i64\",\"key_names\":[],\"key_types\":[],\"type\":\"currency_stats\"}],\"ricardian_clauses\":[],\"error_messages\":[],\"abi_extensions\":[],\"variants\":[]}";

    //     public static string json = "{\"from\":\"............1\",\"to\":\"emteamacc222\",\"quantity\":\"1.0000 GPE\",\"memo\":\"i m test\"}";

    //     public static string contract = "eosio.token";
    //     public static string name = "transfer";
    //     public static string type = null;
    // }

    class Program
    {

        [DllImport(@"../cpp/libabieos-lib")]
        public static extern IntPtr em_create();

        [DllImport(@"../cpp/libabieos-lib")]
        public static extern void em_destroy(IntPtr context);

        [DllImport(@"../cpp/libabieos-lib")]
        public static extern ulong em_string_to_name(IntPtr context, string nameStr);

        [DllImport(@"../cpp/libabieos-lib")]
        public static extern bool em_set_abi(IntPtr context, ulong contract, [MarshalAs(UnmanagedType.LPUTF8Str)] string abiStr);

        [DllImport(@"../cpp/libabieos-lib")]
        public static extern bool em_json_to_bin(IntPtr context, ulong contract, string type, [MarshalAs(UnmanagedType.LPUTF8Str)] string json, bool reorderable);

        [DllImport(@"../cpp/libabieos-lib")]
        public static extern IntPtr em_get_bin_hex(IntPtr context);

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            IntPtr context = em_create();

            bool res;

            ulong contract_ = em_string_to_name(context, data.contract);
            Console.WriteLine($"contract:{contract_}");

            res = em_set_abi(context, contract_, data.abi);
            Console.WriteLine($"res:{res}");

            res = em_json_to_bin(context, contract_, data.type, data.json, true);
            Console.WriteLine($"res:{res}");

            IntPtr hex = em_get_bin_hex(context);
            try
            {
                Console.WriteLine($"hex:{Marshal.PtrToStringAnsi(hex)}");
            }
            finally
            {
                Marshal.FreeHGlobal(hex);
            }
            //LoopTest(context);

            em_destroy(context);
            Console.WriteLine("Done!");
        }

        static void LoopTest(IntPtr context)
        {
            int c = 0;
            while (c++ >= 0)
            {
                IntPtr hex = em_get_bin_hex(context);
                try
                {
                    if (c % 10000 == 0)
                        Console.WriteLine($"count:{c}");
                }
                finally
                {
                    Marshal.FreeHGlobal(hex);
                }
            }
        }
    }
}
